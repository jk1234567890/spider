{-# OPTIONS -Wall #-}
{-# LANGUAGE Arrows, Rank2Types, TypeOperators #-}
module Main where

import Control.Applicative as Applicative
import Control.Arrow       as Arrow

       -- Data
import Data.Record        as Record
import Data.Record.Optionality        as RO

    -- FRP.Grapefruit
import FRP.Grapefruit.Signal as Sig
import FRP.Grapefruit.Signal.Discrete  as DSignal
import FRP.Grapefruit.Signal.Segmented as SSignal

    -- Graphics.UI.Grapefruit
import Graphics.UI.Grapefruit.Item          as UIItem
import Graphics.UI.Grapefruit.Circuit       as UICircuit
import Graphics.UI.Grapefruit.Backend.Basic as BasicUIBackend
import Graphics.UI.Grapefruit.GTK as GTK

import System.Random(mkStdGen,RandomGen)
import System.Random.Shuffle
import Data.Time.Format (defaultTimeLocale,formatTime)
import Data.Time(getCurrentTime)
import Data.List(foldl')

questions :: [(String, String)]
questions = [
  (">>=", "Monad m => m a -> a -> m b"),
  (">>", "Monad m => m a -> m b"),
  ("liftM2", "Monad m => (a -> b) -> m a -> m b"),
  (">>>", "Arrow a => a b c -> a c d -> a b d"),
  (">=>", "Monad m => (a -> m b) -> (b -> m c) -> a -> m c")
  ]


prepareQuestions :: RandomGen gen => [(String, String)] -> gen -> ([String], [String], [Int])
prepareQuestions qq gen =
  (fst <$> qq, (snd.(qq!!).(+(-1))) <$> ord, (((length qq)+1)-) <$> ord)
  where
    ord = shuffle' [1..(length questions)] (length questions) gen

main :: IO ()
main = do
  gen <- mkStdGen <$> (read <$> formatTime defaultTimeLocale "%s" <$> getCurrentTime)
  run GTK (mainCircuit (prepareQuestions questions gen)) ()

emphasize :: Int -> String -> Int -> String
emphasize myVal text currVal
 | myVal == currVal = "**"++text++"**"
 | otherwise = text

appendId :: String -> Int -> String
appendId text idx = (show idx)++":"++text

funcs :: (BasicUIBackend uiBackend) =>
  [String]
  -> UICircuit Widget uiBackend era (SSignal era Int) (DSignal era Int)
funcs [] = arr $ const $ DSignal.empty
funcs (fun:xs) = proc selected -> do
  sig <- funcs xs -< selected
  X :& Push := pf <- just pushButton -< X :& Text := emphasize ((length xs)+1) (appendId fun ((length xs)+1)) <$> selected
  returnA -< (DSignal.union (DSignal.map (const ((length xs)+1)) pf) sig)

data Selection

data Evt = Click | Change Int

updateEvt :: (Int, Int) -> Evt -> (Int, Int)
updateEvt (last, _) Click = (last, last)
updateEvt (_, accp) (Change new) = (new, accp)

types :: (BasicUIBackend uiBackend) =>
  [String]
  -> UICircuit Widget uiBackend era (SSignal era Int) ((DSignal era (), [SSignal era Int]))
types [] = arr $ const $ (DSignal.empty , [])
types (tt:xs) = proc ssig -> do
    rec let
          push = (const Click) <$> pf
          vv = Change <$> updates ssig
          sampl = snd <$> SSignal.scan (0,0) updateEvt (union push vv)
        (sig, ss) <- types xs -< ssig
        X :& Push := pf <- just pushButton -< X :& Text := appendId tt <$> sampl
    returnA -< (DSignal.union sig pf, sampl:ss)


gameAreaInner :: (BasicUIBackend uiBackend) =>
  [String] -> [String]
  -> UICircuit Widget uiBackend era () (SSignal era [Int])
gameAreaInner ff tt = proc () ->  do
  rec let
        signal = (SSignal.construct 0 (DSignal.union sig (const 0 <$> ssig)))
      X `With` sig <- (BasicUIBackend.box Vertical `with` funcs ff) -< X `With` signal
      X `With` (ssig, state) <- BasicUIBackend.box Vertical `with` types tt -< X `With` signal
  returnA -< foldr (liftA2 (:)) (pure []) state

gameArea :: (BasicUIBackend uiBackend) =>
  [String] -> [String]
  -> UICircuit Widget uiBackend era () (SSignal era [Int])
gameArea ff tt = proc () -> do
  X `With` sig <- BasicUIBackend.box Horizontal `with` gameAreaInner ff tt -< X `With` ()
  returnA -< sig

countAnswers :: [Int] -> [Int] -> Int
countAnswers a b = foldl' (+) 0 $ Prelude.map ee $ zip a b
  where
    ee :: (Int, Int) -> Int
    ee (x,y)
      | x == y = 1
      | otherwise = 0

resultArea :: (BasicUIBackend uiBackend) =>
  [Int] -> UICircuit Widget uiBackend era (SSignal era [Int]) ()
resultArea ans = proc currAns -> do
  X <- just label -< X :& Text := ("Poprawnych odpowiedzi: "++) . show . countAnswers ans <$> currAns
  returnA -< ()


mainCircuit :: (BasicUIBackend uiBackend) =>
  ([String],[String],[Int]) ->
  UICircuit Window uiBackend era () (DSignal era ())
mainCircuit (questions, answers, ansMap) = proc () -> do
  X :& Closure := closure `With` (X  `With` _) <- window `with` (BasicUIBackend.box Vertical `with` layout) -< X :& Title := (pure "Haskell game") `With` (X `With` ())
  returnA -< closure
  where
    layout = (gameArea questions answers) >>> resultArea ansMap 
